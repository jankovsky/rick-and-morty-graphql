import React from 'react';

import {copyTextToClipboard} from '../../libs/dom';

export const CopyButton: React.FC = () => {
    const handleOnClick = (): void => {
        copyTextToClipboard('copyText')
    }

    return (
        <button onClick={handleOnClick}>Copy!</button>
    )
}
