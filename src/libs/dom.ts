// Copy to clipboard
function fallbackCopyTextToClipboard(text: string): void {
    const textArea: HTMLTextAreaElement = document.createElement('textarea')
    textArea.value = text

    // Avoid scrolling to bottom
    textArea.style.top = '0'
    textArea.style.left = '0'
    textArea.style.position = 'fixed'

    document.body.appendChild(textArea)
    textArea.focus()
    textArea.select()

    try {
        const successful = document.execCommand('copy')
        const msg = successful ? 'successful' : 'unsuccessful'
        console.log(`Fallback: Copying text command was ${msg}`)
    } catch (err) {
        console.error('Fallback: Oops, unable to copy', err)
    }

    document.body.removeChild(textArea)
}

export function copyTextToClipboard(text: string): void {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text)
        return
    }
    navigator.clipboard.writeText(text).then(function(): void {
        console.log('Async: Copying to clipboard was successful!')
    }, function(err): void {
        console.error('Async: Could not copy text: ', err)
    })
}