import React from 'react';

import {Search} from './components/Search/Search';
import {CopyButton} from './components/CopyButton/CopyButton';
import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
          <Search />
          <CopyButton />
      </header>
    </div>
  );
}

export default App;
